7.2 Go 中的指针有什么作用？
===========================

普通的变量，存储的是数据，而指针变量，存储的是数据的内存地址。

学习指针，主要有两个运算符号，要记牢

-  ``&``\ ：地址运算符，从变量中取得值的内存地址

.. code:: go

   // 定义普通变量并打印
   age := 18
   fmt.Println(age) //output: 18

   ptr := &age
   fmt.Println(ptr) //output: 

-  ``*``\ ：解引用运算符，从内存地址中取得存储的数据

.. code:: go

   myage := *ptr
   fmt.Println(myage) //output: 18
